package com.grupoasesores.repository;

import java.util.List;

import com.grupoasesores.model.DataDTO;
import com.grupoasesores.model.RequestIn;

public interface IRepo {
	public List<DataDTO> consultaData();
	public int insertar(RequestIn requestIn) throws Exception;
	public int actualizar(RequestIn requestIn) throws Exception;
	public DataDTO consultaElemento(String idArticulo);
}
