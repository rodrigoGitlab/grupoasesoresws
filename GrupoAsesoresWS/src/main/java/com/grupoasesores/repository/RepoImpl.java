package com.grupoasesores.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.grupoasesores.model.DataDTO;
import com.grupoasesores.model.RequestIn;

@Repository
@Qualifier("repo2")
public class RepoImpl implements IRepo{
	
	private static Logger LOG=LoggerFactory.getLogger(RepoImpl.class);
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	public int actualizar(RequestIn requestIn) throws Exception{
    	LOG.info("RepoImpl.actualizar");
    	int estatusUpdate=-1;
    	try {
    		estatusUpdate=jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                	PreparedStatement ps =connection.prepareStatement("UPDATE datatable SET descripcion = ? , modelo = ? where idArticulo = ?");
    	    			ps.setString(1,requestIn.getDescripcion());
    	    			ps.setString(2,requestIn.getModelo());
    	    			ps.setString(3,requestIn.getIdArticulo());
                    return ps;
                }
            });
            
            LOG.info("RepoImpl.insertar..estatusUpdate.."+estatusUpdate);
    	}catch(Exception e) {
    		LOG.info("RepoImpl.insertar..Exception al actualizar en tabla datatable.."+e.getMessage());
    		e.printStackTrace();
    		throw new Exception("OTPRepoImpl.insertar..Exception al actualizar en tabla datatable.."+e.getMessage());
    	}    	
    	return estatusUpdate;
    }
	
	public int insertar(RequestIn requestIn) throws Exception{
    	LOG.info("RepoImpl.insertar");
    	int estatusInserta=-1;
    	try {
            estatusInserta=jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                	PreparedStatement ps =connection.prepareStatement("INSERT INTO datatable (idArticulo,nombre,descripcion,precio,modelo) VALUES (?,?,?,?,?)");
    	    			ps.setString(1,requestIn.getIdArticulo());
    	    			ps.setString(2,requestIn.getNombre());
    	    			ps.setString(3,requestIn.getDescripcion());
    	    			ps.setString(4,requestIn.getPrecio());
    	    			ps.setString(5,requestIn.getModelo());
                    return ps;
                }
            });
            
            LOG.info("RepoImpl.insertar..estatusInserta.."+estatusInserta);
    	}catch(Exception e) {
    		LOG.info("RepoImpl.insertar..Exception al insertar en tabla datatable.."+e.getMessage());
    		e.printStackTrace();
    		throw new Exception("OTPRepoImpl.insertar..Exception al insertar en tabla datatable.."+e.getMessage());
    	}    	
    	return estatusInserta;
    }
    
    public List<DataDTO> consultaData() {
    	LOG.info("RepoImpl.consultaData");
    	List<DataDTO> list=new ArrayList<DataDTO>();
    	try {
    		list = jdbcTemplate.query(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement("select idArticulo,nombre,descripcion,precio,modelo from datatable");
                    return ps;
                }
            }, new ResultSetExtractor< List<DataDTO> >() {
                @Override
                public List<DataDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {
                	List<DataDTO> listTemp=new ArrayList<DataDTO>();
                	
                	DataDTO dataDTO = new DataDTO();
                	while (rs.next()) {
                		LOG.info("WWWWWWWW.."+rs.getString("idArticulo"));
                        LOG.info("WWWWWWWW.."+rs.getString("nombre"));
                        LOG.info("WWWWWWWW.."+rs.getString("descripcion"));
                        LOG.info("WWWWWWWW.."+rs.getString("precio"));
                        LOG.info("WWWWWWWW.."+rs.getString("modelo"));

                        dataDTO.setIdArticulo(rs.getString("idArticulo"));
                        dataDTO.setNombre(rs.getString("nombre"));
                        dataDTO.setDescripcion(rs.getString("descripcion"));				
                        dataDTO.setPrecio(rs.getString("precio"));
                        dataDTO.setModelo(rs.getString("modelo"));
                        dataDTO.setEstatusCodigo("0");
                        dataDTO.setDescripcionError("Exito, encontro informacion en tabla datatable");
                        listTemp.add(dataDTO);
                        dataDTO = new DataDTO();
                    }
                    return listTemp;
                }
                
            });
    		
    		LOG.info("list.size().."+list.size());
    		    		
    		if(list.isEmpty()) {
    			LOG.info("RepoImpl.consultaData..dataDTO==null");
    			DataDTO dataDTO=new DataDTO();
    			dataDTO.setEstatusCodigo("-1");
    			dataDTO.setDescripcionError("Error en Consulta");
    			list.add(dataDTO);
            }
    		return list;
    	}catch(Exception e) {
    		LOG.info("RepoImpl.consultaData..Exception e.."+e.getMessage());
        	e.printStackTrace();
        	DataDTO dataDTO=new DataDTO();
        	dataDTO.setEstatusCodigo("-1");
        	dataDTO.setDescripcionError("RepoImpl.consultaData..Exception e.."+e.getMessage());
        	list.add(dataDTO);
        	return list;
    	}

    }
    
    public DataDTO consultaElemento(String idArticulo) {
    	LOG.info("RepoImpl.consultaElemento");
    	DataDTO dataDTO = null;
    	
    	try {
    		dataDTO = jdbcTemplate.query(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement ps = con.prepareStatement("select idArticulo,nombre,descripcion,precio,modelo from datatable where idArticulo = ?");
                    ps.setString(1, idArticulo);
                    return ps;
                }
            }, new ResultSetExtractor< DataDTO >() {
                @Override
                public DataDTO extractData(ResultSet rs) throws SQLException, DataAccessException {
                                	
                	DataDTO dataDTOTemp = new DataDTO();
                	while (rs.next()) {
                		LOG.info("WWWWWWWW.."+rs.getString("idArticulo"));
                        LOG.info("WWWWWWWW.."+rs.getString("nombre"));
                        LOG.info("WWWWWWWW.."+rs.getString("descripcion"));
                        LOG.info("WWWWWWWW.."+rs.getString("precio"));
                        LOG.info("WWWWWWWW.."+rs.getString("modelo"));

                        dataDTOTemp.setIdArticulo(rs.getString("idArticulo"));
                        dataDTOTemp.setNombre(rs.getString("nombre"));
                        dataDTOTemp.setDescripcion(rs.getString("descripcion"));				
                        dataDTOTemp.setPrecio(rs.getString("precio"));
                        dataDTOTemp.setModelo(rs.getString("modelo"));
                        dataDTOTemp.setEstatusCodigo("0");
                        dataDTOTemp.setDescripcionError("Exito, encontro informacion en tabla datatable");
                    }
                    return dataDTOTemp;
                }
                
            });
    		    		
    		if(dataDTO==null) {
    			LOG.info("RepoImpl.consultaData..dataDTO==null");
    			dataDTO=new DataDTO();
    			dataDTO.setEstatusCodigo("-1");
    			dataDTO.setDescripcionError("Error en Consulta");
            }
    		return dataDTO;
    	}catch(Exception e) {
    		LOG.info("RepoImpl.consultaData..Exception e.."+e.getMessage());
        	e.printStackTrace();
        	dataDTO=new DataDTO();
        	dataDTO.setEstatusCodigo("-1");
        	dataDTO.setDescripcionError("RepoImpl.consultaData..Exception e.."+e.getMessage());
        	return dataDTO;
    	}

    }
}
