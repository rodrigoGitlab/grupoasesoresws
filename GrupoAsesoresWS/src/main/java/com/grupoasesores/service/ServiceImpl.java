package com.grupoasesores.service;

import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.grupoasesores.model.DataDTO;
import com.grupoasesores.model.RequestIn;
import com.grupoasesores.repository.IRepo;

@Service
public class ServiceImpl implements IService{
	private static Logger LOG=LoggerFactory.getLogger(ServiceImpl.class);
	
	@Autowired
	@Qualifier("repo2")
	private IRepo repo;

	@Override
	public String consultaData() {
		LOG.info("Service.consultaData");
		List<DataDTO> list=repo.consultaData();		
		String data="";
		Iterator<DataDTO> iter=list.iterator();
		while(iter.hasNext()) {
			data+=((DataDTO)iter.next()).toString()+"\n";
		}
		return data;
	}

	@Override
	public int insertar(RequestIn requestIn) {
		LOG.info("Service.insertar");
		int res=-1;
		try {
			res = repo.insertar(requestIn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public String consultaElemento(String idArticulo) {
		LOG.info("Service.consultaElemento");
		DataDTO dataDTO=repo.consultaElemento(idArticulo);
		String res=dataDTO.toString();
		return res;
	}

	@Override
	public int actualizar(RequestIn requestIn) {
		LOG.info("Service.actualizar");
		int res=-1;
		try {
			res = repo.actualizar(requestIn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
}
