package com.grupoasesores.service;

import com.grupoasesores.model.RequestIn;

public interface IService {    
	public int insertar(RequestIn requestIn);
	public int actualizar(RequestIn requestIn);
    public String consultaData();
    public String consultaElemento(String idArticulo);
}
