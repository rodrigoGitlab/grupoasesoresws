package com.grupoasesores.mx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.grupoasesores"})
public class WsApplication extends SpringBootServletInitializer{	
	private static Logger LOG=LoggerFactory.getLogger(WsApplication.class);
	
	public static void main(String[] args) {;
		SpringApplication.run(WsApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(WsApplication.class);
	}

}
