package com.grupoasesores.rest;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.grupoasesores.model.RequestIn;
import com.grupoasesores.model.RespuestaDTO;
import com.grupoasesores.service.IService;

@RestController
public class ServiceRS {
	private static Logger logger=LoggerFactory.getLogger(ServiceRS.class);

	@Autowired
	private IService service;
	
	@RequestMapping(value = "/consultaData", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<RespuestaDTO> consultaData(){
		logger.info("ServiceRS.consultaData");
		String data=service.consultaData();
		RespuestaDTO respuestaDTO=new RespuestaDTO("0","exito.."+ data );		
		return new ResponseEntity<RespuestaDTO>(respuestaDTO,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/consultaElemento", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<RespuestaDTO> consultaElemento(@RequestBody RequestIn requestIn){
		logger.info("ServiceRS.consultaElemento..requestIn.getIdArticulo().."+requestIn.getIdArticulo());
		String data=service.consultaElemento(requestIn.getIdArticulo());
		RespuestaDTO respuestaDTO=new RespuestaDTO("0","exito.."+ data );		
		return new ResponseEntity<RespuestaDTO>(respuestaDTO,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/insertaData", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<RespuestaDTO> generaOTP(@RequestBody RequestIn requestIn){
		logger.info("ServiceRS..insertaData");
		logger.info("ServiceRS..insertaData..getIdArticulo.."+requestIn.getIdArticulo());
		logger.info("ServiceRS..insertaData..getNombre.."+requestIn.getNombre());
		logger.info("ServiceRS..insertaData..getDescripcion.."+requestIn.getDescripcion());
		logger.info("ServiceRS..insertaData..getPrecio.."+requestIn.getPrecio());
		logger.info("ServiceRS..insertaData..getModelo.."+requestIn.getModelo());
		int res=service.insertar(requestIn);
		logger.info("res: "+res);
		
		RespuestaDTO respuestaDTO=new RespuestaDTO("0", "Exito al insertar.."+res);
		return new ResponseEntity<RespuestaDTO>(respuestaDTO,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/actualizaData", method = RequestMethod.POST, produces="application/json")
	public ResponseEntity<RespuestaDTO> actualizaData(@RequestBody RequestIn requestIn){
		logger.info("ServiceRS..actualizaData");
		logger.info("ServiceRS..actualizaData..getIdArticulo.."+requestIn.getIdArticulo());
		logger.info("ServiceRS..actualizaData..getNombre.."+requestIn.getNombre());
		logger.info("ServiceRS..actualizaData..getDescripcion.."+requestIn.getDescripcion());
		logger.info("ServiceRS..actualizaData..getPrecio.."+requestIn.getPrecio());
		logger.info("ServiceRS..actualizaData..getModelo.."+requestIn.getModelo());
		int res=service.actualizar(requestIn);
		logger.info("res: "+res);
		
		RespuestaDTO respuestaDTO=new RespuestaDTO("0", "Exito al actualizar.."+res);
		return new ResponseEntity<RespuestaDTO>(respuestaDTO,HttpStatus.OK);
	}
}
