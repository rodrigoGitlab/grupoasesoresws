package com.grupoasesores.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class RespuestaDTO {
	private String estatus;
	private String descripcion;
	private String timestamp;
	
	public RespuestaDTO(String estatus, String descripcion) {
		super();
		this.estatus = estatus;
		this.descripcion = descripcion;
		this.timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Timestamp(System.currentTimeMillis()));
	}	
	
	@Override
	public String toString() {
		return "RespuestaDTO [estatus=" + estatus + ", descripcion=" + descripcion + ", timestamp=" + timestamp
				+ ", getEstatus()=" + getEstatus() + ", getDescripcion()=" + getDescripcion() + ", getTimestamp()="
				+ getTimestamp() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	

}
