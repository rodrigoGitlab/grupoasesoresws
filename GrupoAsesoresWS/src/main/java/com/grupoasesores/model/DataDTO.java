package com.grupoasesores.model;

public class DataDTO {
	private String estatusCodigo;
	private String descripcionError;
	private String idArticulo;
	private String nombre;
	private String descripcion;
	private String precio;
	private String modelo;
	
	
	
	public String getEstatusCodigo() {
		return estatusCodigo;
	}
	public void setEstatusCodigo(String estatusCodigo) {
		this.estatusCodigo = estatusCodigo;
	}
	public String getDescripcionError() {
		return descripcionError;
	}
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}
	public String getIdArticulo() {
		return idArticulo;
	}
	public void setIdArticulo(String idArticulo) {
		this.idArticulo = idArticulo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getPrecio() {
		return precio;
	}
	public void setPrecio(String precio) {
		this.precio = precio;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	@Override
	public String toString() {
		return "DataDTO [estatusCodigo=" + estatusCodigo + ", descripcionError=" + descripcionError + ", idArticulo="
				+ idArticulo + ", nombre=" + nombre + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", modelo=" + modelo + "]";
	}
	
	
}
